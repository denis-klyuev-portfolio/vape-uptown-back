create table liquid_flavors (
	liquid_id int NOT NULL,
	flavor_id int NOT NULL,
	volume int NOT NULL default 1,
	PRIMARY KEY (liquid_id, flavor_id)
);
insert into liquid_flavors (liquid_id, flavor_id, volume) values
(1,23,1000),
(1,9,600),
(1,27,400),
(1,26,400),
(1,25,200),
(3,15,550),
(3,16,300),
(3,17,300),
(3,18,300),
(3,19,300);
