create table vendors (
	id int NOT NULL AUTO_INCREMENT,
	name varchar(250) NOT NULL,
	PRIMARY KEY (id)
);
INSERT INTO vendors (id, name) VALUES
	(1,'TPA/TFA'),
	(2,'Capella');
