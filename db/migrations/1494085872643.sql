ALTER TABLE liquids
	ADD best_in_days int NOT NULL DEFAULT 0,
	ADD description TEXT DEFAULT NULL,
	ADD favorited_counter int NOT NULL DEFAULT 0,
	ADD vg_part int NOT NULL DEFAULT 7000,
	ADD ad_part int NOT NULL DEFAULT 0,
	ADD nic_part int NOT NULL DEFAULT 0,
	ADD CONSTRAINT CHK_vg_part CHECK (vg_part>=0 AND vg_part <= 10000),
	ADD CONSTRAINT CHK_ad_part CHECK (ad_part>=0 AND ad_part <= 10000),
	ADD CONSTRAINT CHK_nic_part CHECK (nic_part>=0 AND nic_part <= 10000);
