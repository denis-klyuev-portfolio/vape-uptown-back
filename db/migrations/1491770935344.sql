create table sessions (
	id char(32) NOT NULL,
	user_id bigint,
	best_before timestamp,
	PRIMARY KEY (id)
);
