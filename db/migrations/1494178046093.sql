create table users (
	id bigint NOT NULL AUTO_INCREMENT,
	email text NOT NULL,
	oauth_user_id varchar(255) NOT NULL,
	oauth_provider int NOT NULL,
	oauth_token text NOT NULL,
	oauth_expire_timestamp int NOT NULL,
	nickname varchar(250) DEFAULT NULL,
	PRIMARY KEY (id),
	UNIQUE KEY oauth_primary_key (oauth_provider, oauth_user_id)
);
