create table liquid_hashtags (
	liquid_id int NOT NULL,
	hashtag_id int NOT NULL,
	PRIMARY KEY (liquid_id, hashtag_id)
);
INSERT INTO liquid_hashtags (liquid_id, hashtag_id) values
(1, 2),
(1, 11),
(1, 12),
(3, 12),
(3, 2),
(3, 14),
(3, 11);
