const path = require('path'),
	fs = require('fs');

const db = require('../db');
const files = fs
	.readdirSync(path.resolve('db/migrations'))
	.filter(f => f.match(/^\d{2,}\.sql$/))
	.map(f => parseInt(f.match(/^(\d+)\.sql$/)[1], 10));

files.sort((a, b) => a - b);

let currentMigration = 0;
const nextMigration = () => {
	if (!files[currentMigration]) {
		return;
	}

	let migrationName = files[currentMigration++];
	let migrationDDL = fs.readFileSync(`db/migrations/${migrationName}.sql`, 'utf-8').trim();
	if (!migrationDDL.length) {
		return Promise.resolve();
	}

	return db.query('SELECT * FROM __migrations__ WHERE migration = ?', migrationName)
		.then(results => !results.length
			? Promise.resolve()
			: Promise.reject())
		.then(() => {
			console.log(`executing migration ${migrationName}.sql`);
			return db.query(migrationDDL)
				.then(
					() => db.query('INSERT INTO __migrations__ (migration, t) VALUES (?, now())', migrationName),
					(err) => console.log(`migration ${migrationName} runned with error: ${err.message}`)
				);
		}, () => {
			console.log(`migration ${migrationName} already applied`);
		})
		.then(nextMigration);
};
db.query('show tables')
	.then(rows => {
		const found = rows.reduce((p, c) =>
			p || Object.keys(c).reduce((_p, _c) => _p || c[_c] === '__migrations__', false)
			, false);
		if (!found) {
			return db.query(fs.readFileSync(`db/migrations/0.sql`, 'utf-8').trim());
		}
	})
	.then(() => nextMigration())
	.then(() => db.end());
