const path = require('path');
const fs = require('fs');

const name = Date.now();
fs.writeFileSync(path.resolve(`db/migrations/${name}.sql`), '', 'UTF-8');
