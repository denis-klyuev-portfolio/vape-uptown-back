const mysql = require('mysql');
const envDev = require('./env-dev.json');

const pool = mysql.createPool({
	connectionLimit: 10,
	host: envDev.mysql.host,
	user: envDev.mysql.user,
	password: envDev.mysql.pass,
	database: envDev.mysql.db,
	charset: 'UTF8_GENERAL_CI',
	multipleStatements: true
});

module.exports = {
	escape: value => pool.escape(value),
	end: () => pool.end(),
	query: (sql, ...values) => {
		return new Promise((resolve, reject) => {
			pool.getConnection((error, connection) => {
				if (error) {
					reject(new Error(error));
				}
				connection.query(sql, values, function (error, results, fields) {
					connection.release();
					if (error) {
						reject(new Error(error));
					} else {
						resolve(results);
					}
				});
			});
		});
	}
};
