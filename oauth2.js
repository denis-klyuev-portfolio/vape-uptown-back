const db = require('./db');
const path = require('path');
const fetch = require('node-fetch');

const config = {
	gg: {
		client_id: '282341406517-47dnrjl8si2qbmp0ub73i1ps8osh5mf5.apps.googleusercontent.com',
		client_secret: 'S3CYlVHB-wdli6lqdI8I0nnH'
	}
};

const stringifyParams = obj => Object
	.entries(obj)
	.map(([key, value]) => `${key}=${encodeURIComponent(value)}`)
	.join('&');

module.exports = function (req, res) {
	const canonicUrl = req.url.replace(/\?.+/, '');
	const redirect_uri = `http://${req.headers.host}/auth/gg/callback`;
	const codeToToken = code => fetch('https://www.googleapis.com/oauth2/v4/token', {
		method: 'POST',
		body: stringifyParams({
			code,
			client_id: config.gg.client_id,
			client_secret: config.gg.client_secret,
			redirect_uri,
			grant_type: 'authorization_code'
		}),
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		}
	});
	const tokenToUserData = token => fetch(`https://www.googleapis.com/oauth2/v3/tokeninfo?access_token=${encodeURIComponent(token)}`);

	switch (canonicUrl) {
		case '/logout':
			delete req.session.user;
			res.redirect('/');
			break;

		case '/login/gg':
			const url = 'https://accounts.google.com/o/oauth2/v2/auth?' +
				stringifyParams({
					redirect_uri,
					response_type: 'code',
					client_id: config.gg.client_id,
					prompt: 'consent',
					include_granted_scopes: 'true',
					scope: [
						'https://www.googleapis.com/auth/userinfo.email',
						'https://www.googleapis.com/auth/userinfo.profile'
					].join(' ')
				});
			res.redirect(url);
			break;

		case '/gg/callback':
			const {code} = req.query;
			if (typeof code === 'string' && code.trim().length) {

				codeToToken(code)
					.then(response => response.json())
					.then(json => {
						if (json.error) {
							throw new Error();
						}
						const {access_token} = json;

						return tokenToUserData(access_token)
							.then(response => response.json())
							.then(json => ({access_token, json}));
					})
					.then(({access_token, json}) => {
						if (json.error || json.aud !== config.gg.client_id) {	//	 TODO: || !json.email_verified
							throw new Error();
						}

						//	that's that moment when all is done except database stuff
						res.status(200);
						res.contentType('text/plain');
						const {email, sub: userId, exp: expireTimestamp} = json;

						db.query(`
						INSERT INTO users (email, oauth_user_id, oauth_provider, oauth_token, oauth_expire_timestamp)
						VALUES (?, 1, ?, ?, ?)
						ON DUPLICATE KEY UPDATE email=?, oauth_token=?, oauth_expire_timestamp=?`, email, userId.toString(), access_token, expireTimestamp, email, access_token, expireTimestamp)
							.then(({insertId}) => db.query('SELECT * FROM users WHERE id = ?', insertId))
							.then(([user]) => {
								req.session.user = {
									id: user.id,
									nickname: user.nickname
								};

								return db.query('UPDATE sessions SET user_id = ?, best_before = now() + interval 7 day WHERE id = ?', user.id, req.session.id);
							})
							.then(() => {
								res.redirect('/');
							});
					})
					.catch(() => {
						res.status(500);
						res.send(`something went wrong`);
					})
			}
			break;

		default:
			res.status(404);
			break;
	}
};
