const db = require('./db');
const path = require('path');

const liquidDataExtractor = ({
								 id, name, best_in_days: bestInDays, description, favorited_counter: favoritedCounter, vg_part: vgPart, nic_part: nicPart, ad_part: adPart
							 }) => ({
	id, name, bestInDays, description, favoritedCounter, vgPart, nicPart, adPart
});

module.exports = function (req, res) {
	res.status(200);

	if (req.method === 'GET') {
		//	TODO: development => api.html; production => index.html
		res.sendFile(path.resolve('./public/api.html'));
		return;
	}
	if (req.method !== 'POST') {
		return;
	}

	res.contentType('application/json');
	const result = {
		result: true
	};
	const sendResult = () => res.send(JSON.stringify(result));

	switch (req.body.method) {	//	from base "/api" router rule
		case 'flavors.list':
			db.query('SELECT * from flavors')
				.then(list => {
					result.list = list.map(
						({id, name, vendor_id: vendorId}) => ({id, name, vendorId}));
				}, () => {
					result.result = false;
				})
				.then(sendResult);
			break;

		case 'vendors.list':
			db.query('SELECT * from vendors')
				.then(list => {
					result.list = list.map(
						({id, name}) => ({id, name}));
				}, () => {
					result.result = false;
				})
				.then(sendResult);
			break;

		case 'liquids.search':
			let {name: liquidName, flavors: flavorIds, hashtags: hashtagIds} = req.body.params;
			liquidName = (liquidName || '').trim().toLowerCase();
			const where = [
				...(liquidName.length ? [`POSITION(${db.escape(liquidName)} IN LOWER(name))`] : []),
				... (flavorIds.length ? [`id IN (SELECT liquid_id FROM liquid_flavors WHERE flavor_id IN (${flavorIds.join(',')}) GROUP BY liquid_id HAVING COUNT(liquid_id) = ${flavorIds.length})`] : []),
				... (hashtagIds.length ? [`id IN (SELECT liquid_id FROM liquid_hashtags WHERE hashtag_id IN (${hashtagIds.join(',')}) GROUP BY liquid_id HAVING COUNT(liquid_id) = ${hashtagIds.length})`] : [])
			];
			db.query(where.length
				? `SELECT * from liquids WHERE ${where.join(' AND ')}`
				: 'SELECT * from liquids')
				.then(list =>
						Promise.all(list
							.map(liquidDataExtractor)
							.map(
								liquid =>
									Promise.all([
										db.query('SELECT flavor_id, volume FROM liquid_flavors WHERE liquid_id = ?', liquid.id)
											.then(flavors => flavors.map(({flavor_id: id, volume}) => ({id, volume}))),
										db.query('SELECT hashtag_id FROM liquid_hashtags WHERE liquid_id = ?', liquid.id)
											.then(hashtags => hashtags.map(hashtag => hashtag.hashtag_id))
									])
										.then(([flavors, hashtags]) => Object.assign({}, liquid, {flavors, hashtags}))
							))
							.then(list => {
								result.list = list;
								// .map(
								// 	({id, name}) => ({id, name})
								// );
							})
					, () => {
						result.result = false;
					})
				.then(sendResult);
			break;

		case 'liquids.oneById':
			const {id: flavorId} = req.body.params;
			db.query('SELECT * from liquids WHERE id = ?', flavorId)
				.then(list =>
						Promise.all(list
							.map(liquidDataExtractor)
							.map(liquid =>
								Promise.all([
									db.query('SELECT flavor_id, volume FROM liquid_flavors WHERE liquid_id = ?', liquid.id)
										.then(flavors => flavors.map(({flavor_id: id, volume}) => ({id, volume}))),
									db.query('SELECT hashtag_id FROM liquid_hashtags WHERE liquid_id = ?', liquid.id)
										.then(hashtags => hashtags.map(hashtag => hashtag.hashtag_id))
								])
									.then(([flavors, hashtags]) => Object.assign({}, liquid, {flavors, hashtags}))
							))
							.then(list => {
								result.item = list[0];
							})
					, () => {
						result.result = false;
					})
				.then(sendResult);
			break;

		case 'user.data':
			result.data = req.session.user || null;
			sendResult();
			break;


		case 'user.saveNickname':
			const {nickname = ''} = req.body.params;
			//	TODO: security & other checks!!!
			if (req.session.user && req.session.user.id) {
				db.query('UPDATE users SET nickname = ? WHERE id = ?', nickname, req.session.user.id)
					.then(() => {
						req.session.user.nickname = nickname;
						result.data = req.session.user || null;
					})
					.then(sendResult);
			} else {
				sendResult();
			}
			break;

		default:
			sendResult();
	}
};
