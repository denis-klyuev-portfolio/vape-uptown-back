require("babel-polyfill");

const path = require('path'),
	express = require('express'),
	session = require('express-session'),
	bodyParser = require('body-parser'),
	app = express(),
	apiMiddleware = require('./api'),
	oauth2Middleware = require('./oauth2'),
	md5 = require('md5'),
	db = require('./db');

app.use(bodyParser.json());
app.use(session({
	secret: 'keyboard cat',
	path: '/',
	httpOnly: true,
	secure: false,
	resave: true,
	saveUninitialized: true,
	maxAge: 24 * 60 * 60 * 1000 // 24 hours
}));
app.use('*', (req, res, next) => {
	if (!req.session.id) {
		req.session.id = md5(Date.now() * Math.round(Math.random() * 1000000));
	}

	db
		.query('SELECT * FROM sessions WHERE id = ?', req.session.id)
		.then(results =>
			new Promise((resolve, reject) => {
				if (results.length) {
					resolve(results[0]);
				} else {
					reject();
				}
			})
		)
		.then(session => {
				if (session.user_id && !req.session.user) {
					return db
						.query('SELECT * FROM users WHERE id = ?', session.user_id)
						.then(results => {
							if (results.length) {
								req.session.user = {id: results[0].id, nickname: results[0].nickname};
							}
						});
				} else {
					return null;
				}
			}, () => db.query(`INSERT INTO sessions (id, best_before) VALUES (?, now() + interval 7 day)`, req.session.id)
		)
		.then(() => next());
});
app.use('/auth', oauth2Middleware);
app.use('/api', apiMiddleware);
app.use(express.static(path.join(__dirname, 'public')));

const server = app.listen(3001, function () {
	const {address: host, port} = server.address();

	console.log('Static HTTP server is listening at http://%s:%s', host, port);
});
